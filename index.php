<!DOCTYPE html>
<html>
<head>
	<title>Http Angular</title>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body ng-app="myApp" ng-controller="dataCtrl">
	<table border="1">
		<tr>
			<th>No</th>
			<th>Nim</th>
			<th>Nama</th>
		</tr>
		<tr ng-repeat="x in mahasiswa">
			<td>{{$index + 1}}</td>
			<td>{{x.nim}}</td>
			<td>{{x.nama | uppercase}}</td>
		</tr>
	</table>
	<script>
		var app = angular.module('myApp', []);
		app.controller('dataCtrl', function($scope, $http) {
			$http.get("mahasiswa.json").then(function(response) {
				$scope.mahasiswa = response.data.mahasiswa;
			});
		});
	</script>
</body>
</html>